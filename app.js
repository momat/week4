var express = require('express');
/*<==== body-parser deals with the post request read the data and put them inside an object===>*/
var bodyParser = require('body-parser');
var app = express();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

//We telling Express to use static file(html and images in this case)
app.use(express.static("views"));
app.use(express.static("images"));
app.use(express.static("css"));
//middlewares: expand express capabalilties to execute some tasks, 
//they get executed before the handlers(get, put).

//parse apps x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

//parse apps in json format
app.use(bodyParser.json());

let db = [];

app.get('/', function(req, res){
    console.log('Thank you for the request');
    res.render('index.html');
});

app.get('/newtask', function(req, res){
    res.render('newtask.html');
});

app.get('/listtasks', function(req, res){
    res.render('listtasks.html', {taskDB:db});
});
app.post('/newtask', function(req, res){
    //let rb = req.body;
    var rec ={
        tName: req.body.taskname,
        tDue: req.body.taskdue,
        tDesc: req.body.taskdesc
    };
    db.push(rec);
    res.render('index.html');
});

app.listen(8080);